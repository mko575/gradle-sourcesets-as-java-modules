package com.gitlab.mko575.gssasjm.test;

import com.gitlab.mko575.gssasjm.HelloWorldFromMain;

import org.junit.jupiter.api.Test;

public class NonModuleTests {

  @Test
  void testApp() {
    System.out.println("Testing App");
    HelloWorldFromMain.methodToBeTested();
  }
}
