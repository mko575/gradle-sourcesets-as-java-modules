package com.gitlab.mko575.gssasjm.lib;

import com.gitlab.mko575.gssasjm.lib.internals.GreetingsStringImpl;

public class GreetingsString {

  public static String get() {
    return GreetingsStringImpl.get("en");
  }

}
