package com.gitlab.mko575.gssasjm.lib.internals;

public class GreetingsStringImpl {

  public static String get(String lang) {
    switch (lang) {
      case "en": return "Hello, World!";
      default: return "???";
    }
  }

}
