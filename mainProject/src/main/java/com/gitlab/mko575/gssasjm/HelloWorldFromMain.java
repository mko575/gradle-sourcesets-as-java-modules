package com.gitlab.mko575.gssasjm;

import com.gitlab.mko575.gssasjm.lib.GreetingsString;

public class HelloWorldFromMain {

    public static void methodToBeTested() {
        System.out.println("Executing the method to be tested from HelloWorldFromMain.");
    }

    public static String getGreetingsString() {
        return GreetingsString.get();
    }

    public static void main(String[] args) {
        System.out.println(getGreetingsString());
    }
}
