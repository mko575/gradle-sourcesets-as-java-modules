#!/bin/bash

# While waiting to configure Gradle run

echo -e "\n=== Running the module version ===\n"
java -p mainProject/build/libs -m GSSasJM.app/com.gitlab.mko575.gssasjm.app.HelloWorldFromApp

echo -e "\n=== Running the non-module version ===\n"
java -cp mainProject/build/libs/mainProject-0.0.1.jar:mainProject/build/libs/GSSasJM.lib@0.0.1.jar com.gitlab.mko575.gssasjm.HelloWorldFromMain
